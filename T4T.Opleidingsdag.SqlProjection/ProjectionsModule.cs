﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace T4T.LevelUp.MilestoneTracking.Projections
{
    public abstract class ProjectionsModule<TConnection>
    {
        private readonly Dictionary<Type, Func<TConnection, object, long, Task>> _handlers;

        protected ProjectionsModule()
        {
            _handlers = new Dictionary<Type, Func<TConnection, object, long, Task>>();
        }

        /// <summary>
        /// Registers a new asynchronous message handler for the given message.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message.</typeparam>
        /// <param name="handler">The handler.</param>
        protected void Register<TMessage>(Func<TConnection, TMessage, long, Task> handler)
        {
            if (handler == null)
                throw new ArgumentNullException(nameof(handler));

            if (_handlers.ContainsKey(typeof(TMessage)))
                throw new InvalidOperationException($"There is already a handler registered for message of type {typeof(TMessage).FullName}!");

            _handlers.Add(typeof(TMessage), (connection, message, position) => handler(connection, (TMessage)message, position));
        }

        /// <summary>
        /// Finds the registered handler for the given message (if any) and invokes it.
        /// </summary>
        /// <typeparam name="TMessage">The type of the message to project.</typeparam>
        /// <param name="message">The message to project.</param>
        /// <param name="connection">The connection.</param>
        public async Task HandleAsync<TMessage>(TMessage message, TConnection connection, long position)
        {
            if (_handlers.TryGetValue(message.GetType(), out var handler))
                await handler.Invoke(connection, message, position);
        }
    }
}
