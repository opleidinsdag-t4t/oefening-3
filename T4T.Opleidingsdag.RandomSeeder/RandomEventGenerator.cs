﻿using System;
using System.Collections.Generic;
using Bogus;
using T4T.Opleidingsdag.RandomSeeder.Events;

namespace T4T.Opleidingsdag.RandomSeeder
{
    public class RandomEventGenerator
    {
        private static readonly List<Type> EventTypes = new List<Type>
        {
            typeof(BalanceIncreased),
            typeof(BalanceDecreased),
            typeof(OwnerAddedToBalance)
        };

        private static readonly List<Guid> BalanceIds = new List<Guid>
        {
            Guid.NewGuid(),
            Guid.NewGuid(),
            Guid.NewGuid(),
            Guid.NewGuid()
        };

        private static readonly Dictionary<Type, Func<object>> Fakers = new Dictionary<Type, Func<object>>();
        private static readonly Random rnd = new Random();

        public RandomEventGenerator()
        {
            GenerateFakers();
        }

        public RandomEvent GenerateRandomEvent()
        {
            var randomEventType = EventTypes[rnd.Next(EventTypes.Count)];

            return new RandomEvent
            {
                Type = randomEventType,
                Event = Fakers[randomEventType]()
            };
        }

        private static void GenerateFakers()
        {
            var balanceIncreasedFaker = new Faker<BalanceIncreased>()
                .RuleFor(e => e.BalanceId, f => BalanceIds[rnd.Next(BalanceIds.Count)])
                .RuleFor(e => e.Amount, f => f.Random.Double(-5000D, 5000D));

            Fakers.Add(typeof(BalanceIncreased), () => balanceIncreasedFaker.Generate(1));

            var balanceDecreasedFaker = new Faker<BalanceDecreased>()
                .RuleFor(e => e.BalanceId, f => BalanceIds[rnd.Next(BalanceIds.Count)])
                .RuleFor(e => e.Amount, f => f.Random.Double(-5000D, 5000D));

            Fakers.Add(typeof(BalanceDecreased), () => balanceDecreasedFaker.Generate(1));

            var ownerAddedFaker = new Faker<OwnerAddedToBalance>()
                .RuleFor(e => e.BalanceId, f => BalanceIds[rnd.Next(BalanceIds.Count)])
                .RuleFor(e => e.OwnerId, f => f.Random.Guid());

            Fakers.Add(typeof(OwnerAddedToBalance), () => ownerAddedFaker.Generate(1));
        }
    }
}
