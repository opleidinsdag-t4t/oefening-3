﻿using System;

namespace T4T.Opleidingsdag.RandomSeeder
{
    public class RandomEvent
    {
        public Type Type { get; set; }
        public object Event { get; set; }
    }
}
