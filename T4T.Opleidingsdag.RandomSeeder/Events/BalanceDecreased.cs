﻿using System;

namespace T4T.Opleidingsdag.RandomSeeder.Events
{
    public class BalanceDecreased
    {
        public Guid BalanceId { get; set; }
        public double Amount { get; set; }
        public long Position { get; set; }
    }
}
