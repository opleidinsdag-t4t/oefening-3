﻿using System;

namespace T4T.Opleidingsdag.RandomSeeder.Events
{
    public class OwnerAddedToBalance
    {
        public Guid BalanceId { get; set; }
        public Guid OwnerId { get; set; }
    }
}
