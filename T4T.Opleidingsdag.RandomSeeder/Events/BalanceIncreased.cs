﻿using System;

namespace T4T.Opleidingsdag.RandomSeeder.Events
{
    public class BalanceIncreased
    {
        public Guid BalanceId { get; set; }
        public double Amount { get; set; }
        public long Position { get;set; }
    }
}
