﻿using System;
using System.Threading;
using Newtonsoft.Json;
using SqlStreamStore;
using SqlStreamStore.Streams;

namespace T4T.Opleidingsdag.RandomSeeder
{
    class Program
    {
        private static readonly ManualResetEvent ResetEvent = new ManualResetEvent(false);

        static void Main(string[] args)
        {
            Console.WriteLine("Starting RandomSeeder...");

            Console.CancelKeyPress += (sender, eventArgs) => ResetEvent.Set();

            var streamId = new StreamId("Foo");
            var randomEventGenerator = new RandomEventGenerator();

            using (var store =
                new MsSqlStreamStore(new MsSqlStreamStoreSettings("Server=.;Database=T4T.Opleidingsdag.Oefening3;Trusted_Connection=True;")))
            {
                if (!store.CheckSchema().Result.IsMatch())
                    store.CreateSchema().Wait();

                var timer = new System.Timers.Timer(5000);
                timer.Elapsed += (sender, timerArgs) => { GenerateRandomEvent(store, randomEventGenerator, streamId); };
                timer.Enabled = true;

                timer.Start();

                ResetEvent.WaitOne();
            }
        }

        private static void GenerateRandomEvent(MsSqlStreamStore store, RandomEventGenerator randomEventGenerator, StreamId streamId)
        {
            var randomEvent = randomEventGenerator.GenerateRandomEvent();
            var jsonData = JsonConvert.SerializeObject(randomEvent.Event);
            var streamMessage = new NewStreamMessage(Guid.NewGuid(), randomEvent.Type.FullName, jsonData);

            Console.WriteLine($"Appending {randomEvent.Type.Name} to stream.");

            store.AppendToStream(streamId, ExpectedVersion.Any, new[]
            {
                streamMessage
            });
        }
    }
}
